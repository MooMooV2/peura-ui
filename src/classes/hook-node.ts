import { HookNodeValue } from '../interfaces/hook';
import { Hook } from './hook';

export class HookNode<H extends HookNodeValue = HookNodeValue> {

  private readonly value: HookNodeValue = {};
  private readonly hooks: Record<string, HookNode | Hook> = {}

  public getValue(): H {
    return this.value as H;
  }

  public setValue(value: any): void {
    const hookNames = Object.getOwnPropertyNames(this.value);
    hookNames.forEach(hookName => {
      const hookValue = value && value[hookName];
      if (hookValue != undefined) {
        this.value[hookName] = hookValue;
      }
    });
  }

  public getHookNode(name: string): HookNode | Hook | undefined {
    return this.hooks[name];
  }

  public getHookNodes(): Record<string, HookNode | Hook> {
    return this.hooks;
  }

  public createHookNode(name: string): HookNode;
  public createHookNode(name: string, hook: Hook): Hook;
  public createHookNode(name: string, hook?: Hook): Hook | HookNode {
    if (!this.hooks[name]) {
      const hookNode = hook ? hook : new HookNode();
      this.hooks[name] = hookNode;
    }
    const childNode = this.hooks[name];
    Object.defineProperty(this.value, name, {
      get: (): HookNodeValue => childNode.getValue(),
      set: (value: any) => {
        childNode.setValue(value);
      }
    });
    return childNode;
  }
}
