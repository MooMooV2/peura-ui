import { PreElement, PreHook, PreAttribute, PreChildNode } from './interfaces/pre';
import { AttributeType } from './interfaces/hook'

// Takes in a HTML-like string
// Returns custom DOM-like object tree equal to the HTML string
export function getBaseTemplate(templateString: string): PreChildNode[] {
  let state: (c: string) => void = findTagStart;
  let lineCount = 1;
  let columnCount = 0;
  let charIndex = 0;

  const rootElement: PreElement = {
    name: '',
    children: [],
    attributes: []
  };
  const targetElements = [rootElement];

  let elementName = '';
  let textContent = '';
  let nodeHook = '';
  let closeElementName = '';

  let attributeName = '';
  let quoteType = '';
  let attributeContent: (string | PreHook)[] = [];
  let attributePartialValue = '';
  let attributePartialHook = '';

  const tagRegex = /[-a-z0-9+#.]/;
  const spaceRegex = /[\s]/;
  const attrNameRegex = /[-+#.a-zA-Z0-9]/;
  const hookRegex = /[a-zA-Z0-9._]/;

  // Find tag starting character "<" and save a text node if there's one
  function findTagStart(c: string): void {
    if (c === '<') {
      createTextNode();
      state = findTagNext;
    }
    else if (c === '\\') {
      state = findNodeEscape;
    }
    else if (c === '{') {
      createTextNode();
      state = findNodeHookStartSpace;
    }
    else {
      textContent += c;
    }
  }

  function findNodeEscape(c: string): void {
    textContent += c;
    state = findTagStart;
  }

  // Find tag name or mark tag as a closing tag
  function findTagNext(c: string): void {
    if (c === '/') {
      state = findClosingTag;
    }
    else if (c.match(tagRegex)) {
      elementName = c;
      state = findTagName;
    }
    else {
      generalSyntaxError();
    }
  }

  // Complete tag name or end tag
  function findTagName(c: string): void {
    if (c.match(tagRegex)) {
      elementName += c;
    }
    else if (c.match(spaceRegex)) {
      createElement();
      state = findAttributeNameStart;
    }
    else if (c === '/') {
      createElement();
      state = findTagEnd;
    }
    else if (c === '>') {
      createElement();
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  function findClosingTag(c: string): void {
    if (c.match(tagRegex)) {
      closeElementName += c;
    }
    else if (c === '>') {
      closeElement(true);
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  // Find attribute, or tag ending
  function findAttributeNameStart(c: string): void {
    if (c.match(attrNameRegex)) {
      attributeName = c;
      state = findAttributeName;
    }
    else if (c.match(spaceRegex)) {}
    else if (c === '/') {
      state = findTagEnd;
    }
    else if (c === '>') {
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  // Complete attribute name
  function findAttributeName(c: string ): void {
    if (c.match(attrNameRegex)) {
      attributeName += c;
    }
    else if (c === '=') {
      state = findAttributeValueStart;
    }
    else if (c.match(spaceRegex)) {
      state = findAfterAttributeSpace;
    }
    else if (c === '/') {
      createAttribute();
      state = findTagEnd;
    }
    else if (c === '>') {
      createAttribute();
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  // Find "=" character after attribute name or next attribute
  function findAfterAttributeSpace(c: string): void {
    if (c === '=') {
      state = findAttributeValueStart;
    }
    else if (c.match(spaceRegex)) {}
    if (c.match(attrNameRegex)) {
      createAttribute();
      state = findAttributeName;
      attributeName = c;
    }
    else if (c === '/') {
      createAttribute();
      state = findTagEnd;
    }
    else if (c === '>') {
      createAttribute();
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  // Find attribute value begin character { or quote
  function findAttributeValueStart(c: string): void {
    if (c === '"' || c === '\'') {
      state = findAttributeValueText;
      quoteType = c;
    }
    else if (c.match(spaceRegex)) {}
    else {
      generalSyntaxError();
    }
  }

  // Get text content of a attribute value
  function findAttributeValueText(c: string): void {
    if (c === quoteType) {
      createPartialAttribute();
      createAttribute();
      state = findAttributeNameStart;
    }
    else if (c === '{') {
      createPartialAttribute();
      state = findAttributeValueHook;
    }
    else if (c === '\\') {
      state = findAttributeEscape;
    }
    else {
      attributePartialValue += c;
    }
  }

  // Save a hook in attribute value
  function findAttributeValueHook(c: string): void {
    if (c.match(hookRegex)) {
      attributePartialHook += c;
    }
    else if (c === '}') {
      createPartialAttributeHook();
      state = findAttributeValueText;
    }
    else {
      generalSyntaxError();
    }
  }

  // Save escaped characters
  function findAttributeEscape(c: string): void {
    attributePartialValue += c;
    state = findAttributeValueText;
  }

  // End and create a tag
  function findTagEnd(c: string): void {
    if (c === '>') {
      closeElement();
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  function findNodeHookStartSpace(c: string): void {
    if (c.match(hookRegex)) {
      nodeHook += c;
      state = findNodeHook;
    }
    else if (c.match(spaceRegex)) {}
    else {
      generalSyntaxError();
    }
  }

  // Find a hook in a text node
  function findNodeHook(c: string): void {
    if (c.match(hookRegex)) {
      nodeHook += c;
    }
    else if (c.match(spaceRegex)) {
      state = findNodeHookEndSpace;
    }
    else if (c === '}') {
      createNodeHook();
      state = findTagStart;
    }
    else {
      generalSyntaxError();
    }
  }

  function findNodeHookEndSpace(c: string): void {
    if (c === '}') {
      createNodeHook();
      state = findTagStart;
    }
    else if (c.match(spaceRegex)) {}
    else {
      generalSyntaxError();
    }
  }

  function createTextNode(): void {
    if (textContent.trim()) { // TODO This potetially loses intended spaces between elements
      const text = textContent.replace('\n', ' ').replace('\t', ' ');
      targetElements[0].children.push(text);
    }
    textContent = '';
  }

  function createNodeHook(): void {
    if (nodeHook.trim()) {
      const element = targetElements[0];
      const hook = { path: nodeHook.split('.') };
      if (element.name.toUpperCase() === 'TEXTAREA') { // HTML speksi LÄTISEEEE
        element.attributes.push(getAttribute(element.name, 'value', [hook]));
      }
      else {
        element.children.push(hook);
      }
    }
    nodeHook = '';
  }

  function createPartialAttribute(): void {
    if (attributePartialValue.trim()) {
      const text = attributePartialValue.replace('\n', ' ').replace('\t', ' ');
      attributeContent.push(text);
      attributePartialValue = '';
    }
  }

  function createPartialAttributeHook(): void {
    attributeContent.push({ path: attributePartialHook.split('.') });
    attributePartialHook = '';
  }

  // Save a HTML attribute in the element
  function createAttribute(): void {
    const element = targetElements[0];
    element.attributes.push(getAttribute(element.name, attributeName, attributeContent));
    attributeName = '';
    attributeContent = [];
  }

  // Create a new HTML tag
  function createElement(): void {
    const element: PreElement = {
      name: elementName,
      attributes: [],
      children: []
    };
    targetElements[0].children.push(element);
    targetElements.splice(0, 0, element);
    elementName = '';
  }

  function closeElement(isClosingTag?: boolean): void {
    if (isClosingTag) {
      if (targetElements[0].name !== closeElementName) {
        closingTagSyntaxError(targetElements[0].name, closeElementName);
      }
      closeElementName = '';
    }
    targetElements.splice(0, 1);
  }

  function closingTagSyntaxError(expected: string, actual: string): void {
    const place = getErrorPlace();
    // eslint-disable-next-line max-len
    throw new Error(`Syntax error in template. Expected to find closing tag for [${expected}] but [${actual}] was found, in line: ${lineCount}, col: ${columnCount + 1}, near [${place}]`);
  }

  function generalSyntaxError(): void {
    const place = getErrorPlace();
    throw new Error(`Syntax error in template, line: ${lineCount}, col: ${columnCount + 1}, near [${place}]`);
  }

  function getErrorPlace(): string {
    const offset = columnCount > 20 ? 20 : columnCount;
    const text = templateString.slice(charIndex - offset, charIndex + 20);
    const indexOfN = text.indexOf('\n');
    const trimAfter = indexOfN === -1 ? text.length : indexOfN;
    return text.substring(0, trimAfter);
  }

  for (; charIndex < templateString.length; charIndex++) {
    const char = templateString[charIndex];
    if (char === '\n') {
      lineCount++;
      columnCount = -1;
    }
    state(char);
    columnCount++;
  }
  return targetElements[0].children;
}

// Thanks a lot HTML/JS specs for making everything so clean and nice
function getAttribute(element: string, attribute: string, value: (string | PreHook)[]): PreAttribute {
  element = element.toUpperCase();
  let type = AttributeType.Normal;
  let name = attribute;
  if ((element === 'INPUT' || element === 'TEXTAREA') && attribute === 'value') {
    type = AttributeType.External;
  }
  else if (element === 'INPUT' && attribute === 'checked') {
    type = AttributeType.External;
  }
  else if (element === 'SELECT' && (attribute === 'selectedindex' || attribute === 'selectedIndex')) {
    type = AttributeType.External;
    name = 'selectedIndex';
  }
  else if (element === 'SELECT' && (attribute === 'selectedoptions' || attribute === 'selectedOptions')) {
    type = AttributeType.Readonly;
    name = 'selectedOptions';
  }
  else if (element === 'OPTION' && attribute === 'selected') {
    type = AttributeType.External;
  }
  else if ((element === 'AUDIO' || element === 'VIDEO') && attribute === 'buffered') {
    type = AttributeType.Readonly;
  }
  return { name, value, type };
}
