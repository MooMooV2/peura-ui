module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  parserOptions: {
    ecmaVersion: 9
  },
  env: {
    browser: true,
    es6: true
  },
  extends: ['plugin:@typescript-eslint/recommended'],
  rules: {
    "semi": 1,
    "no-extra-semi": 1,
    "space-before-function-paren": [1,"never"],
    "keyword-spacing": [1, { "after": true }],
    "no-trailing-spaces": 2,
    "no-unused-vars": [2, { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
    "quotes": [1, "single"],
    "spaced-comment": [1, "always"],
    "no-unreachable": 2,
    "no-undef": [2, { "typeof": true}],
    "func-style": [2, "declaration"],
    "no-multiple-empty-lines": 1,
    "valid-typeof": 2,
    "max-len": [1, { "code": 120 }],
    "no-unneeded-ternary": 2,
    "indent": [2, 2],
    "@typescript-eslint/indent": [2, 2],
    "@typescript-eslint/no-use-before-define": [2, { "functions": false, "classes": true }],
    "@typescript-eslint/no-explicit-any": 0,
    "@typescript-eslint/no-non-null-assertion": 0
  }
}
