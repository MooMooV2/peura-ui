# Peura-UI

## Introduction

Peura-UI is a minimalistic library for rendering user interfaces, a bit similar to Angular or React. Peura-UI also emphasizes the idea of components, similar to the other libraries. The main difference is, it has no automatic render at all. You as a developer are in charge when and what to render.

Isn't this a bit of re-inventing the wheel as there are other similar libraries already available, you ask? Yes and no. I originally started this project for personal learning reasons. But then it accidentally came out quite good so I wanted to share it. There are also a few reasons why one would want to choose this instead of any of the other choices. First, Peura-UI is very lightweight - minified bundle of the whole thing is only 8.5kB. Second, there are times when I have found it frustrating when other libraries are rerendering stuff even when it might be unnecessary. Well manual approach implicitly required also more manual coding, but it's a trade off between more manual code and more automation magic.

## Install

Install with npm:

```bash
npm install --save-dev peura-ui
```

Install with yarn:

```bash
yarn add peura-ui --dev
```

An UMD module is also available on Gitlab for use without npm.

Just run:
```bash
npm install
npm run build
```
And you can find it in ./lib/peura-ui.umd.min.js

## Getting started

The following code is a very simple example of the usage of the library. Lets examine more closely to get the basic concepts. There's also a demo app in the Gitlab repo with more examples.

```import { Component } from 'peura-js';

const template = '<h1>{example.text}</h1>';

class MyComponent extends Component {

  constructor() {
    super(template);
    this.hooks.example.text = 'Hello world';
  }

  afterUnmount() {
    this.hooks.example.text = '';
  }
}

window.addEventListener('load', () => {
  const myComponent = new MyComponent();
  myComponent.appendOn(document.body);
});
```

### Template

Template is basically a string presenting a piece of HTML, but with a little twist. See the part `{example.text}` in the template? It is called a "hook" and is the main way to mutate DOM on the fly. It consists of {} brackets and of a path that is defined by keys separated with dots. We will come back to hooks in just a moment.

### Component

Component is a base class that is used to create your own components (like the MyComponent in the example). It takes the template as an input parameter and creates a DOM tree of it internally. You can also pass initial values for hooks as an optional second parameter. Components can be assigned as value for a hook (not shown in the example) or they can be set manually in the DOM, as the line `myComponent.appendOn(...)` does in the example. Please use only methods (appendOn, clearSelf) of the component. Using javascript's default `element.appendChild` will cause unwanted side effects.

### Hook

Hooks are links between your component instance and the DOM. You can access the hook model by this.hooks property. The hook model is actually pile of getters and setters that manages DOM changes as you assign them with a value. So setting `this.hooks.example.text = '...'` will actually change the value in DOM as well.

### Life cycle hooks

Lastly there are life cycle hooks. They are simple functions that are called after certain changes in the component's state. There are four of them: `beforeMount`, `afterMount`, `beforeUnmount`, and `afterUnmount`. `beforeMount` will be executed just before the component is being added in a parent component. `afterMount` is, not surprisingly, executed after component has been added to the parent. Last two are in similar way, executed when component is being removed of it's parent.

### A word about performance

The one big difference to other popular UI libraries is that hooks mutate DOM directly. Therefore changing value of a hook in a mounted component will cause reflow and repaint in the browser. This can lead to performance problems especially with large arrays. For example if we have a table with 1000 rows, setting rows directly will cause 1000 reflows, yikes! Mutating DOM directly can also have performance benefits though, if we only want to change one small detail on the page. Compared to other popular libraries which rerender whole lot of stuff on every change. Anyhow, it is advisable to perform large changes in an unmounted component and append it as part of DOM only after all changes are done.

## Documentation

Full reference is under construction. Sorry about that :(
