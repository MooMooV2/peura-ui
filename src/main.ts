export { Component } from './classes/component';
export { HookNode } from './classes/hook-node';
export { Hook } from './classes/hook';

export { getBaseTemplate } from './get-base-template';
