import { PreChildNode, PreHook, PreAttribute } from './interfaces/pre';
import { HookParams, AttributeType } from './interfaces/hook';
import { TemplateHookModel, FragElemCapsule } from './interfaces/component';
import { HookNode } from './classes/hook-node';
import { Hook } from './classes/hook';

export function buildTemplateAndHooks(
  preNodes: PreChildNode[],
  root: FragElemCapsule
): TemplateHookModel {
  const hooks = new HookNode();
  const template = buildDomAndHookTree(preNodes, hooks, root);
  return { hooks, template };
}

function buildDomAndHookTree(
  preNodes: PreChildNode[],
  hooks: HookNode,
  parent: FragElemCapsule
): (HTMLElement | Text)[] {
  const nodes = preNodes.reduce((elements, preNode, index) => {
    if (isString(preNode)) {
      elements.push(document.createTextNode(preNode));
    } else if (isPreHook(preNode)) {
      elements.push(document.createTextNode('')); // Placeholder to keep sibling indexes right
      const hookParams = {
        element: parent,
        siblings: elements,
        siblingIndex: index
      };
      iterateHooks(hooks, preNode.path, hookParams);
    } else {
      const element = document.createElement(preNode.name);
      buildAttributes(element, preNode.attributes, hooks);
      buildDomAndHookTree(preNode.children, hooks, { element })
        .forEach(node => {
          element.appendChild(node);
        });
      elements.push(element);
    }
    return elements;
  }, [] as (HTMLElement | Text)[]);

  return nodes.filter(node => node);
}

function buildAttributes(
  element: HTMLElement,
  preAttributes: PreAttribute[],
  hooks: HookNode
): void {
  preAttributes.forEach(preAttribute => {
    const attributeValue = preAttribute.value.reduce((value, valueItem, index) => {
      if (isString(valueItem)) {
        value.push(valueItem);
      } else {
        value.push('');
        iterateHooks(hooks, valueItem.path, {
          element: { element },
          attribute: preAttribute.name,
          siblings: value,
          siblingIndex: index,
          type: preAttribute.type
        });
      }
      return value;
    }, [] as string[]);
    if (preAttribute.type != AttributeType.Readonly) {
      element.setAttribute(preAttribute.name, attributeValue.join(''));
    }
  });
}

function isString(value: PreChildNode): value is string {
  return typeof value === 'string';
}

function isPreHook(value: PreChildNode): value is PreHook {
  return !!(value as PreHook).path; // TODO parempi tarkistus
}

function iterateHooks(hookNode: HookNode, pathPart: string[], hookParams: HookParams): void {
  // Hook or node already exists
  if (hookNode.getHookNode(pathPart[0])) {
    appendHookNode(hookNode, pathPart, hookParams);
  }
  // Create a new hook
  else {
    createHookNode(hookNode, pathPart, hookParams);
  }
}

function appendHookNode(hookNode: HookNode, pathPart: string[], hookParams: HookParams): void {
  // This is not the last item of the path: Pass through
  if (pathPart.length > 1) {
    const childNode = hookNode.getHookNode(pathPart[0]) as HookNode;
    iterateHooks(childNode, pathPart.slice(1, pathPart.length), hookParams);
  }
  // This is the last item of the path: Append params on the existing hook
  else {
    const hook = hookNode.getHookNode(pathPart[0]) as Hook;
    hook.appendParams(hookParams);
  }
}

function createHookNode(hookNode: HookNode, pathPart: string[], hookParams: HookParams): void {
  // This is not the last item of the path: Create hook node
  if (pathPart.length > 1) {
    const childNode = hookNode.createHookNode(pathPart[0]);
    iterateHooks(childNode, pathPart.slice(1, pathPart.length), hookParams);
  }
  // This is the last item of the path: Create hook
  else {
    const hook = new Hook(hookParams);
    hookNode.createHookNode(pathPart[0], hook);
  }
}
