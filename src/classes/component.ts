import { HookNode } from './hook-node';
import { BaseTemplate } from '../interfaces/pre';
import { HookNodeValue } from '../interfaces/hook';
import { FragElemCapsule } from '../interfaces/component';
import { getBaseTemplate } from '../get-base-template';
import { buildTemplateAndHooks } from '../build-template-and-hooks';

export class Component<H extends HookNodeValue = HookNodeValue> {

  private __template: Node[];
  private __hooks: HookNode<H>;
  private __fragment = document.createDocumentFragment();
  private __root: FragElemCapsule = { element: this.__fragment };

  public beforeMount?: () => void;
  public afterMount?: () => void;
  public beforeUnmount?: () => void;
  public afterUnmount?: () => void;

  public get template(): Node[] {
    return this.__template;
  }

  public get hooks(): H {
    return this.__hooks.getValue();
  }

  public set hooks(value: H) {
    this.__hooks.setValue(value);
  }

  public constructor(template: BaseTemplate | string, data?: any) {
    const baseTemplate = typeof template === 'string' ? getBaseTemplate(template) : template;
    const hookTemplateModel = buildTemplateAndHooks(baseTemplate, this.__root);
    this.__template = hookTemplateModel.template;
    this.__hooks = hookTemplateModel.hooks as HookNode<H>;
    hookTemplateModel.template.forEach(node => {
      this.__fragment.appendChild(node);
    });

    // TODO Maybe implement directly in buildTemplateAndHooks for better performance?
    // TODO Could also implement "disposable component" with no hooks at all
    if (data) {
      this.hooks = data;
    }
  }

  public appendOn(element: HTMLElement | DocumentFragment, followingElement?: Node | null): void {
    if (this.beforeMount) {
      this.beforeMount();
    }
    if (followingElement) {
      element.insertBefore(this.__fragment, followingElement);
    }
    else {
      element.appendChild(this.__fragment);
    }
    this.__root.element = element;
    if (this.afterMount) {
      this.afterMount();
    }
  }

  public clearSelf(): void {
    if (this.beforeUnmount) {
      this.beforeUnmount();
    }
    this.__template.forEach(node => {
      this.__fragment.appendChild(node);
    });
    this.__root.element = this.__fragment;
    if (this.afterUnmount) {
      this.afterUnmount();
    }
  }
}
