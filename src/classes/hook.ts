import {
  HookParams,
  ElementHookParams,
  AttributeHookParams,
  ElementSiblings,
  FragElem,
  AttributeType
} from '../interfaces/hook';
import { Component } from './component';

function isAttributeParam(param: HookParams): param is AttributeHookParams {
  return "attribute" in param;
}

export class Hook {

  private value: any;
  private params: HookParams[] = [];

  public constructor(params: HookParams) {
    this.params.push(params);
  }

  public getValue(): any {
    // Read current value in cases it might change
    const param = this.params[0];
    if (isAttributeParam(param) && param.type != AttributeType.Normal) {
      this.value = (param.element.element as any)[param.attribute]
    }

    // In any case return current value
    return this.value;
  }

  public setValue(value: any): void {
    this.value = value;
    this.params.forEach(params => {
      if (isAttributeParam(params)) {
        this.setAttribute(params, value);
      }
      else {
        this.setElement(params, value);
      }
    });
  }

  public appendParams(params: HookParams): void {
    if (isAttributeParam(params) && params.type != AttributeType.Normal) {
      throw new Error("Cannot use hook multiple time for attribute that might change.");
    }
    else {
      this.params.push(params);
    }
  }

  private setElement(params: ElementHookParams, value: any): void {
    const element = params.element.element;
    const ownChildren = params.siblings[params.siblingIndex];
    const followingElement = this.getFollowingElement(params.siblings, params.siblingIndex);

    // Remove old DOM node(s)
    if (ownChildren !== null) {
      if (ownChildren instanceof Component) {
        ownChildren.clearSelf();
      }
      else if (Array.isArray(ownChildren)) {
        ownChildren.forEach(child => {
          if (child instanceof Component) {
            child.clearSelf();
          }
          else {
            element.removeChild(child);
          }
        });
      }
      else {
        element.removeChild(ownChildren);
      }
    }

    // Append new DOM node(s)
    let mapped = value;

    // Use result of the value if it's a function
    if (typeof value === 'function') {
      mapped = value();
    }

    // Convert value into DOM Nodes
    const sibling =
      this.appendElementAndGetSibling(mapped, params.element.element, followingElement);
    if (sibling === null) {
      params.siblings.splice(params.siblingIndex, 1, null);
    }
    else if (sibling.length === 1) {
      params.siblings.splice(params.siblingIndex, 1, sibling[0]);
    }
    else {
      params.siblings.splice(params.siblingIndex, 1, sibling);
    }
  }

  private appendElementAndGetSibling(
    value: any,
    parent: FragElem,
    followingElement: Node | null,
    allowArray = true
  ): (HTMLElement | Text | Component)[] | null {
    const siblings: (HTMLElement | Text | Component)[] = [];
    if (value instanceof Component) {
      value.appendOn(parent, followingElement);
      siblings.push(value);
    }
    else if (value instanceof DocumentFragment) {
      siblings.push(...value.childNodes as any); // This works just fine, WTF TypeScript?
      if (siblings.length > 0) {
        this.appendOn(parent, followingElement, value);
      }
    }
    else if (value instanceof HTMLElement || value instanceof Text) {
      this.appendOn(parent, followingElement, value);
      siblings.push(value);
    }
    else if (Array.isArray(value) && allowArray) {
      value.forEach(item => {
        const mapped = this.appendElementAndGetSibling(item, parent, followingElement, false);
        if (mapped) {
          siblings.push(...mapped);
        }
      });
    }
    else if (value !== undefined && value !== null && value !== '') {
      const textNode = document.createTextNode(value);
      this.appendOn(parent, followingElement, textNode);
      siblings.push(textNode);
    }
    if (siblings.length === 0) {
      return null;
    }
    return siblings;
  }

  private appendOn(element: FragElem, followingElement: Node | null, value: Node): void {
    if (followingElement) {
      element.insertBefore(value, followingElement);
    }
    else {
      element.appendChild(value);
    }
  }

  private getFollowingElement(siblings: ElementSiblings, index: number): Node | null {
    if (index === siblings.length - 1) {
      return null;
    }
    for (let nextIndex = index + 1; nextIndex < siblings.length; nextIndex++) {
      const nextSibling = siblings[nextIndex];
      if (nextSibling) {
        if (Array.isArray(nextSibling)) {
          const foundSibling = nextSibling.find(node => !!node);
          if (foundSibling) {
            if (foundSibling instanceof Component) {
              // TODO Is it possible to create component without content?
              return foundSibling.template[0];
            }
            return foundSibling;
          }
        }
        else if (nextSibling instanceof Component) {
          // TODO Is it possible to create component without content?
          return nextSibling.template[0];
        }
        else {
          return nextSibling;
        }
      }
    }
    return null;
  }

  private setAttribute(param: AttributeHookParams, value: any): void {
    const element = param.element.element;
    const attribute = param.attribute;

    // Special handling for read only properties
    if (param.type == AttributeType.Readonly) {
      throw new Error(`Cannot set readonly attribute: ${attribute}`);
    }

    // Join value into a string if it's an array
    if (Array.isArray(value)) {
      const separator = attribute === 'class' ? ' ' : '';
      value = value.join(separator);
    }

    // The attribute has no siblings
    else if (param.siblings.length === 1) {
      if (param.type == AttributeType.External) {
        (element as any)[attribute] = value;
      }
      else {
        if (value === undefined || value === null || value === false) {
          element.removeAttribute(attribute);
        }
        else if (value === true) {
          element.setAttribute(attribute, '');
        }
        else if (typeof value === 'function') {
          (element as any)[attribute] = value;
        }
        else {
          element.setAttribute(attribute, value);
        }
      }
    }

    // The attribute with multiple siblings implies string type parameter
    else {
      param.siblings.splice(param.siblingIndex, 1, String(value ?? ''));
      const fullValue = param.siblings.join('');
      if (param.type != AttributeType.Normal) {
        (element as any)[attribute] = fullValue;
      }
      else {
        element.setAttribute(attribute, fullValue);
      }
    }
  }
}
