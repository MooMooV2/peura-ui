import { AttributeType } from './hook';

export interface PreElement {
  name: string;
  attributes: PreAttribute[];
  children: PreChildNode[];
}

export type PreChildNode = string | PreElement | PreHook;

export interface PreAttribute {
  name: string;                 // The "id" part in <div id="foo">
  value: (string | PreHook)[];  // The "foo" part in <div id="foo">
  type: AttributeType;          // See AttributeType
}

export interface PreHook {
  path: string[];
}

export type BaseTemplate = PreChildNode[];
