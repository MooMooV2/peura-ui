const Component = PeuraUI.Component;
const getBaseTemplate = PeuraUI.getBaseTemplate;

const introductionTemplate = `
  <h1 class="title">Peura-UI</h1>
  <div class="box">
    Hello, this is a demo app created with Peura-UI. You can find various examples of different use cases below.
  </div>
`;

class IntroductionComponent extends Component {
  constructor() {
    super(introductionTemplate);
  }
}


const basicUseTemplate = `
  <div class="box">
    <div>
      It is possible to change content and attributes (like style here) of HTML elements!
    </div>
    <button onclick="{changeContent}">Change content</button>
    <button onclick="{changeStyle}">Change style</button>
    <p style="color: {color}">{content}</p>
  </div>
`;

class BasicUseComponent extends Component {
  constructor() {
    // Note the use of initial hook values here
    super(basicUseTemplate, { color: 'black', content: 'Some content' });

    // Use wrapper functions here to get the right this-scope
    this.hooks.changeContent = () => this.changeContent();
    this.hooks.changeStyle = () => this.changeStyle();
  }

  changeContent() {
    const content = this.hooks.content === 'Some content' ? 'Modified content' : 'Some content';
    this.hooks.content = content;
  }

  changeStyle() {
    const color = this.hooks.color === 'black' ? 'red' : 'black';
    this.hooks.color = color;
  }
}


const childTemplate = '<div class="child">{value}</div>';
const parentTemplate = `
  <div class="box">
    It is possible to append various types with hooks!
    <table class="table">
      <tr>
        <td><button onclick="{addText}">Add string</button></td>
        <td><span class="code">"Example string"</span></td>
      </tr>
      <tr>
        <td><button onclick="{addNumber}">Add number</button></td>
        <td><span class="code">123.45</span></td>
      </tr>
      <tr>
        <td><button onclick="{addComponent}">Add component</button></td>
        <td><span class="code">new Component(childTemplate, "Child component text")</span></td>
      </tr>
      <tr>
        <td><button onclick="{addTextList}">Add list of strings</button></td>
        <td><span class="code">["foo, ", "bar, ", "baz, ", "glib, ", "glob, "]</span></td>
      </tr>
      <tr>
        <td><button onclick="{addComponentList}">Add list of components</button></td>
        <td><span class="code">
          [
          <br/> 'new Component(childTemplate, \\{ value: "foo" \\})',
          <br/> 'new Component(childTemplate, \\{ value: "bar" \\})',
          <br/> 'new Component(childTemplate, \\{ value: "baz" \\})'
          <br/>]
        </span></td>
      </tr>
    </table>
    <button onclick="{reset}">Reset all</button>

    <div>{text}</div>
    <div>{number}</div>
    <div>{component}</div>
    <div>{textList}</div>
    <div>{componentList}</div>
  </div>
`;

class ParentComponent extends Component {

  constructor() {
    super(parentTemplate);

    this.text = 'Example string';
    this.number = 123.45;
    this.textList = ['foo, ', 'bar, ', 'baz, ', 'glib, ', 'glob, '];
    this.component = new Component(childTemplate, { value: 'Child component text'});
    this.componentList = [
      new Component(childTemplate, { value: 'foo' }),
      new Component(childTemplate, { value: 'bar' }),
      new Component(childTemplate, { value: 'baz' })
    ];

    // Use wrapper functions here to get the right this-scope
    this.hooks.addText = () => this.add('text');
    this.hooks.addNumber = () => this.add('number');
    this.hooks.addComponent = () => this.add('component');
    this.hooks.addTextList = () => this.add('textList');
    this.hooks.addComponentList = () => this.add('componentList');
    this.hooks.reset = () => this.reset();
  }

  add(field) {
    this.hooks[field] = this[field];
  }

  reset() {
    ['text', 'number', 'component', 'textList', 'componentList'].forEach(key => {
      this.hooks[key] = null;
    });
  }
}

const inputTemplate = `
  <div class="box">
    <div>
      This example demonstrates special behaviour of input fields. They're value is updated when user changes value of the input.
    </div>
    <div class="child">
      <div>Input value can be read and written with hooks.</div>
      <input type="text" value="{text.value}" onkeyup="{text.update}"/>
      <button onclick="{text.change}">Reset input</button>
      <div>{text.output}</div>
    </div>
    <div class="child">
      <div>Textarea value can be read and written with hooks.</div>
      <textarea onkeyup="{textarea.update}">{textarea.value}</textarea>
      <button onclick="{textarea.change}">Reset input</button>
      <div>{textarea.output}</div>
    </div>
    <div class="child">
      <div>Checkbox checked value can be read and written with hooks.</div>
      <input type="checkbox" checked="{checkbox.value}" onchange="{checkbox.update}"/>
      <button onclick="{checkbox.change}">Toggle checkbox</button>
      <div>{checkbox.output}</div>
    </div>
    <div class="child" onclick="{radio.update}">
      <div>Radio button state can be read.</div>
      <label>Foo <input type="radio" checked="{radio.0.value}" name="radio" /></label>
      <label>Bar <input type="radio" checked="{radio.1.value}" name="radio" /></label>
      <label>Baz <input type="radio" checked="{radio.2.value}" name="radio" /></label>
      <div>{radio.output}</div>
    </div>
    <div class="child">
      <div>Select dropdown list state can be read and written.</div>
      <select type="radio" selectedindex="{select.value}" onchange="{select.update}">
        <option>Foo</option>
        <option>Bar</option>
        <option>Baz</option>
      </select>
      <div>{select.output}</div>
    </div>
  </div>
`;

class InputComponent extends Component {
  constructor() {
    super(inputTemplate, {
      text: {
        update: () => this.updateText(),
        change: () => this.changeText()
      },
      textarea: {
        update: () => this.updateTextarea(),
        change: () => this.changeTextarea()
      },
      checkbox: {
        update: () => this.updateCheckbox(),
        change: () => this.changeCheckbox()
      },
      radio: {
        update: () => this.updateRadio()
      },
      select: {
        update: () => this.updateSelect()
      },
    });
  }

  updateText() {
    this.hooks.text.output = this.hooks.text.value;
  }

  changeText() {
    this.hooks.text.value = '';
    this.hooks.text.output = '';
  }

  updateTextarea() {
    console.log(this.hooks.textarea.value);
    this.hooks.textarea.output = this.hooks.textarea.value;
  }

  changeTextarea() {
    this.hooks.textarea.value = '';
    this.hooks.textarea.output = '';
  }

  updateCheckbox() {
    this.hooks.checkbox.output = this.hooks.checkbox.value;
  }

  changeCheckbox() {
    const currentValue = this.hooks.checkbox.value;
    this.hooks.checkbox.value = !currentValue;
    this.hooks.checkbox.output = !currentValue;
  }

  updateRadio() {
    const items = ["Foo", "Bar", "Baz"];
    for (let i = 0; i < 3; i++) {
      if (this.hooks.radio[i].value) {
        this.hooks.radio.output = `${i} ${items[i]}`;
        break;
      }
    }
  }

  updateSelect() {
    const items = ["Foo", "Bar", "Baz"];
    this.hooks.select.output = items[this.hooks.select.value]
  }
}


const lifeTimeChildTemplate = `
  <div class="child">
    <div>{title}</div>
    <input type="text" value="{inputValue}" onkeyup="{update}"/>
    <div>{value}</div>
  </div>
`;
const lifeTimeTemplate = `
  <div class="box">
    <div>
      This demonstrates the two different ways to use components - persistent and temporal.
      Make some changes in the child components, toggle them off and back on, and see the difference.
    </div>
    <button onclick="{togglePersistent}">Toggle persistent</button>
    <button onclick="{toggleTemporal}">Toggle temporal</button>
    {persistent}
    {temporal}
  </div>
`;

class LifeTimeChildComponent extends Component {
  constructor(title) {
    super(lifeTimeChildTemplate, { title, update: event => this.update(event) });
  }

  update() {
    this.hooks.value = this.hooks.inputValue;
  }
}

class LifeTimeComponent extends Component {
  constructor() {
    super(lifeTimeTemplate);

    this.persistent = new LifeTimeChildComponent('Persistent component');

    this.hooks.togglePersistent = () => this.togglePersistent();
    this.hooks.toggleTemporal = () => this.toggleTemporal();
    this.hooks.persistent = this.persistent;
    this.hooks.temporal = new LifeTimeChildComponent('Temporal component');
  }

  togglePersistent() {
    this.hooks.persistent = this.hooks.persistent ? null : this.persistent;
  }

  toggleTemporal() {
    this.hooks.temporal = this.hooks.temporal ? null : new LifeTimeChildComponent('Temporal component');
  }
}


const listChildTemplate = '<div class="child">{value}</div>';
const listWrapperTemplate = '<div>{list}</div>';
const listTemplate = `
  <div class="box">
    <div>
      This example demonstrates performance differences between different implementations with large lists.
      Differences between the implementations of these lists are explained in the demo's source code.
    </div>
    <button onclick="{addList1}">Add 1 item (good performance)</button>
    {time1}
    <div class="list-container">{list1}</div>

    <button onclick="{addList2}">Add 1 item (bad performance)</button>
    {time2}
    <div class="list-container">{list2}</div>
  </div>
`;

/* We can pre bake template to make component initialization a bit faster.
  This function is needed to run only once. It convert string template into object format which is then consumed by
  the component. Normally component does this step internally every time it is initialized.
*/
const base = getBaseTemplate(listChildTemplate);

class ListChildComponent1 extends Component {
  constructor(value) {
    super(base, { value });
  }
}

class ListChildComponent2 extends Component {
  constructor(value) {
    super(listChildTemplate, { value });
  }
}

class ListComponent extends Component {
  constructor() {
    super(listTemplate);

    // Good: This adds list items into a wrapper element before it is mounted, so reflow is run only once when
    // the wrapper element is appended in DOM.
    this.list1 = this.getListValues();
    const list1 = this.list1.map(value => new ListChildComponent1(value));
    const listWrapper = new Component(listWrapperTemplate, { list: list1 });
    this.hooks.list1 = listWrapper;

    // Bad: This will add every list item in DOM individually and therefore run reflow every time an item is added.
    // That is, 1000 times.
    this.list2 = this.getListValues();
    const list2 = this.list2.map(value => new ListChildComponent2(value));
    this.hooks.list2 = list2;

    this.hooks.addList1 = () => this.addList1();
    this.hooks.addList2 = () => this.addList2();
  }

  getListValues() {
    const list = [];
    for (let i = 10000; i > 0; i--) {
      list.push(`Value ${i}`);
    }
    return list;
  }

  // Good: Same reasons as above
  addList1() {
    const a = new Date();
    this.list1.splice(0, 0, `Added value ${this.list1.length + 1}`);
    const list = this.list1.map(value => new ListChildComponent1(value));
    const listWrapper = new Component(listWrapperTemplate, { list });
    this.hooks.list1 = listWrapper;
    const b = new Date();
    this.hooks.time1 = `Took ${b - a} ms`;
  }

  // Bad: Same reasons as above
  addList2() {
    const a = new Date();
    this.list2.splice(0, 0, `Added value ${this.list2.length + 1}`);
    const list = this.list2.map(value => new ListChildComponent2(value));
    this.hooks.list2 = list;
    const b = new Date();
    this.hooks.time2 = `Took ${b - a} ms`;
  }
}


const containerTemplate = '<div>{content}</div>';

class ContainerComponent extends Component {
  constructor() {
    super(containerTemplate);

    const components = [
      new IntroductionComponent(),
      new BasicUseComponent(),
      new ParentComponent(),
      new InputComponent(),
      new LifeTimeComponent(),
      new ListComponent()
    ];

    this.hooks.content = components;
  }
}

window.addEventListener('load', () => {
  const containerComponent = new ContainerComponent();
  containerComponent.appendOn(document.body);
});
