import { HookNode } from '../classes/hook-node';

export interface TemplateHookModel {
  template: (HTMLElement | Text)[];
  hooks: HookNode;
}

export interface ElemCapsule {
  element: HTMLElement;
}

export interface FragElemCapsule {
  element: HTMLElement | DocumentFragment;
}
