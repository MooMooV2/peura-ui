This file describes the behavior of a hook when different types of values are assigned.

Set element hook:
	undefined: remove Node
	null: remove Node
	boolean: set as TextNode
	string: set as TextNode
  	empty: remove Node
	number: set as TextNode
	function: run the function and use result
		Same rules apply for the result as normally, unless it is a
		function: set as TextNode
	array: append every array item using normal rules, except
		null: ignore
		undefined: ignore
		empty string: ignore
		empty array or array with only empty items (listed above): remove Node
	HTMLElement: set as it is
	Text: set as it is
	DocumentFragment: set as it is
	Component: set as it is
	object: set as TextNode


Set attribute hook with 1 value:
	undefined: remove attribute
	null: remove attribute
	true: set as empty string
	false: remove attribute
	string: set as it is
	number: set as string
	array: join with " " as string
	function: set as callback function
	anything else: set as string

Set attribute hook that has multiple values (as siblings):
	undefined: set as empty string
	null: set as empty string
	anything else: set as string

<input value /> has special behaviour
  undefined: set as empty string
  null: set as empty string
  array: join with " " as string
  anything else: set as string

<input checked /> has special behaviour
  all values: cast to boolean