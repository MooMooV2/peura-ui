import { ElemCapsule, FragElemCapsule } from './component';
import { Component } from '../classes/component';

export enum AttributeType {
  Normal = 0,    // Normal attribute
  External = 1,  // Attribute can be modified externally e.g. value in <input>
  Readonly = 2   // Attribute is readonly
}

export type FragElem = HTMLElement | DocumentFragment;

// Interface for hook node tree, hook can return any value
export interface HookNodeValue {
  [key: string]: any;
}

export type ElementSiblings = (Node | Component | (Node | Component)[] | null)[];

export interface ElementHookParams {
  element: FragElemCapsule;
  siblings: ElementSiblings;
  siblingIndex: number;
}

export interface AttributeHookParams {
  element: ElemCapsule;
  attribute: string;
  siblings: (string | boolean | Function)[];
  siblingIndex: number;
  type: AttributeType;
}

export type HookParams = ElementHookParams | AttributeHookParams;

export interface HookSetElements {
  dom: Node | null | Component;
  sibling: Node | Node[] | Component | null;
}
