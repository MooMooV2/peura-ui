const path = require('path');

module.exports = {
  entry: './src/main.ts',
  output: {
    filename: 'peura-ui.umd.min.js',
    path: path.resolve(__dirname, 'lib'),
    libraryTarget: 'umd',
    library: 'PeuraUI',
    umdNamedDefine: true
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: ['/node_modules/', '/demo/']
      }
    ]
  },
  optimization: {
    minimize: true
  }
};
